/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leeftijdVenusJaren;

import java.util.Scanner;

/**
 *
 * @author Jeffrey
 */
public class PracticumOpdracht {
    
    /**
     * @param args the command line arguments
     * Programma om personalia op te vragen en leeftijd in venusjaren
     * te berekenen.
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        final int huidigJaar = 2015;
        String newLine = System.lineSeparator();
        
        //Invoer voor naam.
        System.out.print("Voer uw naam in ");
        String naam = input.next();
        
        //Invoer geboortejaar
        System.out.print("Voer uw geboorte jaar in ");
        int geboortejaar = input.nextInt();
        
        //Bereken leeftijd en Venusjaar
        int leeftijd = huidigJaar - geboortejaar;
        double venusLeeftijd = leeftijd / 0.62;
        
        //Display leeftijd en venusLeeftijd
        System.out.println("Beste " + naam + " in " + huidigJaar +
        " zal jouw leeftijd " + leeftijd + " zijn." + newLine +
           "En je leeftijd in Venusjaren is " + venusLeeftijd);
    }
    
}
